<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Camelon</title>
    <link rel="stylesheet" href="css/style.css">



    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alfa+Slab+One&family=Anton&family=Open+Sans:wght@300&family=Orbitron&family=Oswald:wght@500&family=Playfair+Display:wght@500&family=Quicksand:wght@300&family=Roboto+Slab:wght@300;400&family=Rubik+Glitch&family=Rubik+Microbe&family=Russo+One&family=Signika:wght@300;400&family=Sora:wght@300;400;500;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

</head>

<body>
    <!-- -----------header--------------- -->

    <?php include 'includes/header.php';?>
   

    <!-- -------have a project--------------- -->
    <section class="have-a-project">

        <div class="container">
            <div class="flex-container">
                <div class="content">

                    <h1>Have a project?</h1>
                    <h1>We would love to help.</h1>
                    <p>Mail us: <span>chameleonbuzz@gmail.com</span> </p>
                </div>
                <div class="form">

                    <form action="">

                        <h1>I’m interested in</h1>
                        <div class="checkbox">
                            <input type="checkbox" id="ui/ux" name="ui/ux" value="ui/ux">
                            <label for="ui/ux">UI/UX</label>
                            <input type="checkbox" id="Website" name="Website" value="Website">
                            <label for="Website">Website</label>
                            <input type="checkbox" id="Branding" name="Branding" value="Branding">
                            <label for="Branding">Branding</label>
                            <input type="checkbox" id="Social" name="Social" value="Social">
                            <label for="Social">Social Media Design</label> <br>

                            <input type="checkbox" id="Content" name="Content" value="Content">
                            <label for="Content">Content Creation</label>
                            <input type="checkbox" id="Strategy" name="Strategy" value="Strategy">
                            <label for="Strategy">Strategy & Consulting</label>
                            <input type="checkbox" id="Other" name="Other" value="Other">
                            <label for="Content">Other</label> <br> <br>

                        </div>
                        <div class="form-inn">
                            <label for="name">Your name</label><br>
                            <input type="text" id="name" name="name">
                            <hr> <br> <br>
                            <label for="Mail">Your email</label>
                            <br>
                            <input type="email" id="mail" name="mail">
                            <hr><br><br>
                            <label for="more">Tell us about the project</label><br>
                            <input type="text" id="more" name="more">
                            <hr> <br> <br> <br>

                        </div>
                        <div class="submit">
                            <button>Send request</button>

                        </div>



                    </form>
                </div>
            </div>


        </div>
    </section>
    <!-- -------we are here for you---------- -->
    <section class="we-are-here">
        <div class="container">

            <h1>We are here for you.</h1>
            <div class="blue-flex-box">


                <div class="blue-flex-item1 flex-item">


                    <!-- --------blue-box1------------ -->
                    <div class="blue-box">
                        <div class="blue-box-content">
                            <p>Location</p>
                            <h3>Boca Raton, Florida 33429</h3>
                        </div>
                        <div class="blue-box-img">
                            <img src="img/location.svg" alt="">
                        </div>

                    </div>



                    <!-- --------blue-box2------------ -->
                    <div class="blue-box">
                        <div class="blue-box-content">
                            <p>Email</p>
                            <h3>chameleonbuzz@gmail.com</h3>
                        </div>
                        <div class="blue-box-img">
                            <img src="img/mail.svg" alt="">
                        </div>

                    </div>




                </div>


                <div class="blue-flex-item2  flex-item">
                    <!-- --------blue-box1------------ -->
                    <div class="blue-box">
                        <div class="blue-box-content">
                            <p>Phone</p>
                            <h3>561-208-1666</h3>
                        </div>
                        <div class="blue-box-img">
                            <img src="img/phone.svg" alt="">
                        </div>

                    </div>



                    <!-- --------blue-box2------------ -->
                    <div class="blue-box">
                        <div class="blue-box-content">
                            <p>Time</p>
                            <h3>24 hours</h3>
                        </div>
                        <div class="blue-box-img">
                            <img src="img/time.svg" alt="">
                        </div>

                    </div>



                </div>
            </div>

        </div>


    </section>






    <!-- ----footer------ -->
    <?php include 'includes/footer.php';?>

</body>

</html>