<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Camelon</title>
    <link rel="stylesheet" href="css/style.css">



    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alfa+Slab+One&family=Anton&family=Open+Sans:wght@300&family=Orbitron&family=Oswald:wght@500&family=Playfair+Display:wght@500&family=Quicksand:wght@300&family=Roboto+Slab:wght@300;400&family=Rubik+Glitch&family=Rubik+Microbe&family=Russo+One&family=Signika:wght@300;400&family=Sora:wght@300;400;500;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

</head>

<body>
    <!-- -----------header--------------- -->

    <?php include 'includes/header.php';?>





    <!-- ----pricing section-------------  -->
    <section class="pricing pricing-page">
        <div class="container">


            <div class="sub-head">
                <div class="sub-head1">
                    <h4>Call Center</h4>

                </div>
                <div class="sub-head2">
                    <h4>Design</h4>
                </div>
            </div>
            <!-- flex--container  -->
            <div class="pricing-flex-container">
                <!-- flex-item1 3 -->
                <div class="pricing-flex-item13">
                    <h1>Flex Support</h1>
                    <div class="para">
                        <div class="innerpara">
                            <p>Pay a low monthly subscription fee and get professional customer support when you need it.</p>

                        </div>
                    </div>
                    <hr>
                    <h2>Starting from</h2>
                    <h3>$100</h3>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>24/7 Coverage Available</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Shared Associates</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                </div>
                <!-- flex-item2  -->
                <div class="pricing-flex-item2">
                    <h1>Dedicated Teams</h1>
                    <div class="para">
                        <div class="innerpara">
                            <p>Pay a low monthly subscription fee and get professional customer support when you need it.</p>

                        </div>
                    </div>
                    <hr>
                    <h2>Starting from</h2>
                    <h3>$100</h3>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark-2.svg" alt="" srcset="">
                        </div>
                        <h4>24/7 Coverage Available</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark-2.svg" alt="" srcset="">
                        </div>
                        <h4>Shared Associates</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark-2.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark-2.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                </div>

                <!-- flex-item1 3 -->
                <div class="pricing-flex-item13">
                    <h1>Flex Support</h1>
                    <div class="para">
                        <div class="innerpara">
                            <p>Pay a low monthly subscription fee and get professional customer support when you need it.</p>

                        </div>
                    </div>
                    <hr>
                    <h2>Starting from</h2>
                    <h3>$100</h3>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>24/7 Coverage Available</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Shared Associates</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                    <div class="list1">
                        <div class="tick">
                            <img src="img/check-mark.svg" alt="" srcset="">
                        </div>
                        <h4>Upgrade to Dedicated Teams at any time</h4>
                    </div>
                </div>



            </div>


        </div>
    </section>









    <!-- ----footer------ -->
    <?php include 'includes/footer.php';?>

</body>

</html>