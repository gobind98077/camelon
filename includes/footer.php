 <!-- ----footer------ -->
 <footer>
        <div class="container">
            <div class="footer-flex-container">
                <div class="chameleon">
                    <div class="chamelon-flex">
                        <div class="img">
                            <img src="img/chameleon-logo.svg" alt="">
                        </div>
                        <div class="content">
                            <h4>Chameleon Technologies</h4>
                            <p>Multi-national sales and Customer Service Hub</p>
                        </div>

                    </div>
                    <p>Ready to get more information on how you can put Chameleon to work for your company to find, engage and...... <a href="">read more</a> </p>
                    <div class="icon">
                        <i class="fa-brands fa-square-facebook"></i>
                        <i class="fa-brands fa-square-instagram"></i>
                    </div>
                </div>

                <div class="quick">
                    <h3>Quick Links</h3>
                    <div class="link"><a href="">Service</a></div>
                    <div class="link"><a href="pricing.html">Pricing</a></div>
                    <div class="link"><a href="">About</a></div>

                </div>
                <div class="quick">
                    <h3>Quick Links</h3>
                    <div class="link"><a href="contact_us.html">Contact Us</a></div>
                    <div class="link"><a href="stories.html">Stories</a></div>

                </div>

                <div class="quick">
                    <h3>Contacts</h3>
                    <div class="link"><a href="">Boca Raton, Florida 33429</a></div>
                    <div class="link"><a href="">561-208-1666</a></div>
                    <div class="link"><a href="">chameleonbuzz@gmail.com</a></div>

                </div>

            </div>

            <hr>
            <p>©2022 All Rights Reserved. Designed & Developed by: <a href="">CodeCrewz</a> </p>
        </div>
    </footer>