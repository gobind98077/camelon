<header class="<?= $home ? '' : 'opt-header' ?>">
    <div class="container">
        <div class="navbar">
            <div class="logo">
                <a href="index.php"> <img src="img/chameleon-logo.svg" alt="" srcset=""></a>

            </div>
            <ul class="nav">
                <li class="nav-items"><a href="">Services</a></li>
                <li class="nav-items"><a href="">About</a></li>
                <li class="nav-items"><a href="pricing.php">Pricing</a></li>
                <li class="nav-items"><a href="stories.php">Stories</a></li>
                <li class="nav-items"><a href="contact_us.php" class="button">Contact Us</a></li>
            </ul>

            <div class="menu">
                <i class="fa-solid fa-bars"></i>

            </div>
        </div>
    </div>
    <?php
    if (!$home) {
        ?>
        <!-- --------breadcrumb-------------- -->
        <img class="background" src="img/contact_us.svg" alt="">
        <section class="breadcrumb">
            <div class="content">
                <h1>Contact Us</h1>
                <p>we would love to hear from you.</p>
            </div>
    
        </section>
    
    <?php
    }
    ?>
</header>