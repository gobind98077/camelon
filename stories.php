<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Camelon</title>
    <link rel="stylesheet" href="css/style.css">



    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alfa+Slab+One&family=Anton&family=Open+Sans:wght@300&family=Orbitron&family=Oswald:wght@500&family=Playfair+Display:wght@500&family=Quicksand:wght@300&family=Roboto+Slab:wght@300;400&family=Rubik+Glitch&family=Rubik+Microbe&family=Russo+One&family=Signika:wght@300;400&family=Sora:wght@300;400;500;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

</head>

<body>
    <!-- -----------header--------------- -->

    <?php include 'includes/header.php';?>
  

    <!-- ---------our-stories----------- -->
    <section class="our-stories">
        <div class="container">
            <div class="flex-story-container">
                <div class="item-story-left">
                    <div class="heading-flex">
                        <h1>Our Story</h1>
                        <img src="/img/arrow-down-right.svg" alt="">
                    </div>
                    <p>Mail us: <span>chameleonbuzz@gmail.com</span> </p>
                </div>
                <div class="item-story-right">
                    <h2>Our founder Jenny N started the company because he wanted to establish a company where every tech related task could be done. </h2>
                    <p>"Jenny N and his capable staff at Chameleon have been an invaluable asset to us in our international outsourcing. We are able to get far more bang for our bucks than having to take on this time and labor-intensive task on our own.
                        We chose Chameleon Technologies because of their track record of success and broad experience in help drive performance in my industry. We have seen great results and have never regretted our decision."</p>

                    <div class="founder-flex">
                        <img src="img/founder.svg" alt="">
                        <div class="content">
                            <h3>Jenny N</h3>
                            <p>Founder/Owner</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section class="double-img">
        <div class="container">
            <div class="flex-image-container">
                <div class="image-item">

                    <img src="img/story-img1.svg" alt="image1">

                </div>
                <div class="image-item">
                    <img src="img/story-img2.svg" alt="image2">
                </div>

            </div>
        </div>
    </section>





    <!-- -----customer--section------ -->
    <section class="customer">

        <h1>What our customer say?</h1>


        <div class="customer-flex-container">

            <!-- item-1--------------- -->
            <div class="flex-item1 customer-flex-item">
                <div class="flex-inner">
                    <div class="img">
                        <img src="img/tommy.svg" alt="">
                    </div>
                    <div class="content">
                        <h3>Tommy B</h3>
                        <div class="star">
                            <img src="img/ratings.svg" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <p>"Our company was able to outsource all of our sales and lead generation effort with immediate improvement over doing it ourselves. The professionals at Chameleon have been able to help us care for our present customers"</p>
            </div>

            <!-- item--2   -->
            <div class="flex-item2 customer-flex-item">
                <div class="flex-inner">
                    <div class="img">
                        <img src="img/jeeny.svg" alt="">
                    </div>
                    <div class="content">
                        <h3>Jeeny N Timeshare</h3>
                        <div class="star">
                            <img src="img/ratings.svg" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <p>"Jeeny N and his capable staff at Chameleon have been an invaluable asset to us in our international outsourcing. We are able to get far more bang for our bucks than having to take on this time and labor-intensive task on our own."</p>


            </div>
            <!-- ----item--3--- -->
            <div class="flex-item3 customer-flex-item">
                <div class="flex-inner">
                    <div class="img">
                        <img src="img/larry.svg" alt="">
                    </div>
                    <div class="content">
                        <h3>Larry M</h3>
                        <div class="star">
                            <img src="img/ratings.svg" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <p>"Chameleon Technologies has helped put our company 'on the map'. It has been a tremendous partner for our outsourcing program, both 'near shore' and in state. We have seen a marked improvement "</p>



            </div>

        </div>
    </section>










    <!-- ----footer------ -->
    <?php include 'includes/footer.php';?>

</body>

</html>